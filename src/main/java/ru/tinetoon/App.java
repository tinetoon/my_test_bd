package ru.tinetoon;

import ru.tinetoon.util.MySqlCooection;
import ru.tinetoon.util.SSHConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    public static void main(String[] args) {

        // Создаём новое подключение к серверу
        SSHConnection connectionMyServer = new SSHConnection();

        // Создаём подключение к БД
        MySqlCooection connectionMyBd = new MySqlCooection(connectionMyServer.getSession());

        // Ваш код для работы с базой данных
        try {
            Statement statement = connectionMyBd.getConnectionBd().createStatement();

            String sqlFromTest = "SELECT x.* FROM my_test_bd.test x";
            ResultSet resultSet = statement.executeQuery(sqlFromTest);

            System.out.println("| ID | name | age | jobTitle | salary |");
            while (resultSet.next()) {
                // Обработка каждой записи, например:
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String age = resultSet.getString("age");
                String jobTitle = resultSet.getString("job_title");
                String salary = resultSet.getString("salary");
                System.out.println("| " + id + "  | " + name + " | " + age + " | " + jobTitle + " | " + salary + " |");
                /*System.out.println("ID: " + id
                        + ", name: " + name
                        + ", age: " + age
                        + ", jobTitle: " + jobTitle
                        + ", salary: " + salary);*/
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        // Завершаем подключение к БД
        connectionMyBd.closeConnectionBd();

        // Завершаем подключение к серверу
        connectionMyServer.sessionDisconnect();
    }
}

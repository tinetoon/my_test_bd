package ru.tinetoon.util;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SSHConnection {
    private final String HOST = "78.36.14.87";
    private final int PORT = 50022;
    private final String USER = "ide";
    private final String PASSWORD = System.getenv("IDE_PASSWORD");
    private Session session;

    public SSHConnection() {
        this.session = newSession(USER, HOST, PORT, PASSWORD);
        newConnection(session);
    }

    public static Session newSession(String user, String host, int port, String password) {
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(user, host, port);
        } catch (JSchException e) {
            throw new RuntimeException(e);
        }

        // Устанавливаем пароль для аутентификации
        session.setPassword(password);

        // Указываем параметры для подключения
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);

        return session;
    }
    public static void newConnection(Session session) {
        // Подключаемся к серверу
        try {
            session.connect();
            System.out.println("[INFO] Подключение к серверу выполнено успешно. Начата новая сессия.");
        } catch (JSchException e) {
            System.out.println("[ERROR] Ошибка одключения к серверу");
            throw new RuntimeException(e);
        }
    }
    public void sessionDisconnect() {
        session.disconnect();
        System.out.println("[INFO] Подключение к серверу завершено. Сессия завершена.");
    }
    public Session getSession() {
        return session;
    }
}

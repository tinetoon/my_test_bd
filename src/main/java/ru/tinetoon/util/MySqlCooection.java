package ru.tinetoon.util;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlCooection {
    private Connection connectionBd;
    private final Session session;
    private final String DB_HOST = "localhost";
    private final int DB_PORT = 3306;
    private String DB_URL;
    private final String DB_NAME = "my_test_bd";
    private final String DB_USER = "user_admin";
    private final String DB_PASSWORD = System.getenv("DR_MYSQL_PASSWORD");

    public MySqlCooection(Session session) {
        this.session = session;

        loadDriverBd(); // Загрузка драйвера базы данных
        int forwardedPort = goPortSshTunnel(); // Проброс порта для SSH-туннеля
        DB_URL = "jdbc:mysql://" + DB_HOST + ":" + forwardedPort + "/" + DB_NAME;

        newConnectionBd(); // Установка соединения с базой данных

    }

    private void newConnectionBd() {
        try {
            connectionBd = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            System.out.println("[INFO] Подключение к БД выполнено успешно");
        } catch (SQLException e) {
            System.out.println("[ERROR] Ошибка одключения к БД");
            throw new RuntimeException(e);
        }
    }
    public Connection getConnectionBd() {
        return connectionBd;
    }
    public void closeConnectionBd() {
        try {
            connectionBd.close();
            System.out.println("[INFO] Подключение к БД завершено");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    private int goPortSshTunnel() {
        try {
            return this.session.setPortForwardingL(0, DB_HOST, DB_PORT);
        } catch (JSchException e) {
            System.out.println("[ERROR] Не удалось пробросить порт для SSH-туннеля");
            throw new RuntimeException(e);
        }
    }
    private void loadDriverBd() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("[ERROR] Не удалось загрузить драйвер БД");
            throw new RuntimeException(e);
        }
    }
}
